// alert("Hello World");
// console.log("Hi");
// console.log(3+2)
// Variables
let name = "Zuitt";
name = "Emmanuel";

let age = 22;

console.log(name);

const boilingPoint = 100;
// boilingPoint = 200;

console.log(boilingPoint);
console.log(age + 3); //25
console.log(name + 'hello') //Emmanuelhello

//  Boolean
let isAlive = true;
console.log(isAlive);

// Arrays
let grades = [98, 96, 95, 90];

let batch197 = ["marnel", "jesus"]

let grade1 = 98;
let grade2 = 96;
let grade3 = 95;
let grade4 = 90;

grades[10] = 200

console.log(grades[10]);
let movie = "jaws";
// console.log(song);
let isp;
console.log(isp);

// null vs undefined

let spouse = null;
console.log(spouse);

//  objects - special kind of data type that is used to mimic real world objects

/* 
    syntax:
        let objectName = { 
            property1: keyValue1, 
            property2: keyValue2,
            property3: keyValue3
        }
*/

let myGrades = {
    firstGrading: 98,
    secondGrading: 92,
    thirdGrading: 90,
    fourthGrading: 94.6
}
console.log(myGrades);

let person = {
    fullName: 'Alonzo Cruz',
    age: 25,
    isMarried: false,
    contact: ['0919212121', '90923091032'],
    address: {
        houseNumber: 345,
        city: 'Manila'
    }
}

console.log(person);